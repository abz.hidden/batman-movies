package com.karimian.abz.batmanmovies.data.repository

import com.karimian.abz.batmanmovies.data.api.Api
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor(
    private val api: Api
) {
    suspend fun getMovieList() = api.getMovieList()

    suspend fun getMovieDetail(imdbId: String) = api.getMovieDetail(imdbId =imdbId)

}