package com.karimian.abz.batmanmovies.data.model

import com.google.gson.annotations.SerializedName


data class SearchResponse(
    @SerializedName("Search")
    val search : List<Movie>
)
