package com.karimian.abz.batmanmovies.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.karimian.abz.batmanmovies.data.model.Movie
import com.karimian.abz.batmanmovies.data.model.MovieDetail
import com.karimian.abz.batmanmovies.data.model.SearchResponse

@Database(entities = [Movie::class,MovieDetail::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun moviesDao(): MovieDao
    abstract fun DetailDao(): DetailDao
}