package com.karimian.abz.batmanmovies.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.karimian.abz.batmanmovies.R
import com.karimian.abz.batmanmovies.databinding.FragmentMovieDetailBinding
import com.karimian.abz.batmanmovies.helper.Resource
import com.karimian.abz.batmanmovies.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailFragment : Fragment() {

    private lateinit var binding: FragmentMovieDetailBinding
    private val viewModels: MovieDetailViewModel by viewModels()
    private val args by navArgs<MovieDetailFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModels.getMovieDetail(args.imdbId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
    }

    private fun observer() {
        binding.apply {
            viewModels.movieDetail.observe(viewLifecycleOwner) {
                when (it) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                            binding.apply {
                                txtTitle.text = it.data.title
                                txtYear.text = it.data.year
                                Glide.with(imgPoster)
                                    .load(it.data.poster)
                                    .into(imgPoster)
                                txtRated.text = it.data.rated
                                txtReleased.text = it.data.released
                                txtRuntime.text = it.data.runtime
                                txtGenre.text = it.data.genre
                                txtDirector.text = it.data.director
                                txtWriter.text = it.data.writer
                                txtActors.text = it.data.actors
                                txtPlot.text = it.data.plot
                                txtLanguage.text = it.data.language
                                txtCountry.text = it.data.country
                                txtAwards.text = it.data.awards
                                txtImdbRating.text = it.data.imdbRating
                                txtImdbVotes.text = it.data.imdbVotes
                                txtBoxOffice.text = it.data.boxOffice
                            }
                    }

                    is Resource.Failure -> {
                        back()
                    }
                }
            }
        }
    }

    private fun back() {
        showErrorMessage()
        requireActivity().onBackPressed()
    }

    private fun showErrorMessage() {
        Toast.makeText(requireContext(),resources.getString(R.string.error_message),Toast.LENGTH_LONG).show()
    }
}