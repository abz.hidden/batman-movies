package com.karimian.abz.batmanmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tbl_movie")
data class Movie(

    @ColumnInfo(name = "title")
    @SerializedName("Title")
    var title: String,

    @ColumnInfo(name = "year")
    @SerializedName("Year")
    var year: String,

    @PrimaryKey
    @ColumnInfo(name = "imdb_id")
    @SerializedName("imdbID")
    var imdbId: String,

    @ColumnInfo(name = "type")
    @SerializedName("Type")
    var type: String,

    @ColumnInfo(name = "poster")
    @SerializedName("Poster")
    var poster: String,


    )
