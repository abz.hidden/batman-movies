package com.karimian.abz.batmanmovies.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.karimian.abz.batmanmovies.R
import com.karimian.abz.batmanmovies.databinding.FragmentMovieListBinding
import com.karimian.abz.batmanmovies.helper.Resource
import com.karimian.abz.batmanmovies.view.adapter.MovieAdapter
import com.karimian.abz.batmanmovies.viewmodel.MovieListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieListFragment : Fragment() {

    private lateinit var binding: FragmentMovieListBinding
    private val viewModels: MovieListViewModel by viewModels()
    private lateinit var movieAdapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModels.getMovies()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        observe()
    }

    private fun setupView() {
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        movieAdapter = MovieAdapter {
                showDetail(it.imdbId)
        }
        binding.rvMovies.adapter = movieAdapter
    }

    private fun observe() {
        binding.apply {
            viewModels.movies.observe(viewLifecycleOwner) {
                when (it) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        movieAdapter.submitList(it.data)
                    }
                    is Resource.Failure -> {}
                }
            }
        }
    }

    private fun showDetail(data: String) {
        findNavController().navigate(MovieListFragmentDirections.movieListToMovieDetail(data))
    }

}