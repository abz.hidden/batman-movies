package com.karimian.abz.batmanmovies.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class App : Application()