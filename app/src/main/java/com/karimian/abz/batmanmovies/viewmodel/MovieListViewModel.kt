package com.karimian.abz.batmanmovies.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karimian.abz.batmanmovies.data.db.MovieDao
import com.karimian.abz.batmanmovies.data.model.Movie
import com.karimian.abz.batmanmovies.data.repository.DataRepository
import com.karimian.abz.batmanmovies.helper.NOT_FOUND
import com.karimian.abz.batmanmovies.helper.NetworkHelper
import com.karimian.abz.batmanmovies.helper.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject


@HiltViewModel
class MovieListViewModel @Inject constructor(
    private val movieDao: MovieDao,
    private val repository: DataRepository,
    private val network: NetworkHelper,
): ViewModel() {

    private val _movies = MutableLiveData<Resource<List<Movie>>>(Resource.Initialize)
    val movies: LiveData<Resource<List<Movie>>> = _movies
    fun getMovies() = viewModelScope.launch {
        if (network.hasInternetConnection()) {
            _movies.postValue(Resource.Loading)
            try {
                val response = repository.getMovieList()
                if (response.search.isNotEmpty()) {
                    _movies.postValue(Resource.Success(response.search))
                    movieDao.insert(response.search)
                    return@launch
                }
                _movies.postValue(Resource.Failure(message = NOT_FOUND))
            } catch (e: Exception) {
                _movies.postValue(Resource.Failure(message = e.message.toString()))
            }
            return@launch
        }

        val response = movieDao.getList()
        response?.let {
            _movies.postValue(Resource.Success(response))
            return@launch
        }
        _movies.postValue(Resource.Failure(message = NOT_FOUND))
        return@launch
    }
}

