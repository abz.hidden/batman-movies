package com.karimian.abz.batmanmovies.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karimian.abz.batmanmovies.data.db.DetailDao
import com.karimian.abz.batmanmovies.data.model.MovieDetail
import com.karimian.abz.batmanmovies.data.repository.DataRepository
import com.karimian.abz.batmanmovies.helper.NOT_FOUND
import com.karimian.abz.batmanmovies.helper.NetworkHelper
import com.karimian.abz.batmanmovies.helper.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val detailDao: DetailDao,
    private val repository: DataRepository,
    private val network: NetworkHelper,
) : ViewModel() {

    private val _movieDetail = MutableLiveData<Resource<MovieDetail>>(Resource.Initialize)
    val movieDetail: LiveData<Resource<MovieDetail>> = _movieDetail
    fun getMovieDetail(imdbId: String) = viewModelScope.launch {
        if (network.hasInternetConnection()) {
            _movieDetail.postValue(Resource.Loading)
            try {
                val response = repository.getMovieDetail(imdbId = imdbId)
                _movieDetail.postValue(Resource.Success(response))
                detailDao.insertDetail(response)
                return@launch
            } catch (e: Exception) {
                _movieDetail.postValue(Resource.Failure(message = e.message.toString()))
            }
            return@launch
        }

        val response = detailDao.getDetail(imdbId)
        response?.let {
            _movieDetail.postValue(Resource.Success(response))
            return@launch
        }
        _movieDetail.postValue(Resource.Failure(message = NOT_FOUND))
        return@launch
    }
}
