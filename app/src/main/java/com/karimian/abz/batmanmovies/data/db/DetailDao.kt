package com.karimian.abz.batmanmovies.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.karimian.abz.batmanmovies.data.model.MovieDetail

@Dao
interface DetailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDetail(movieDetail: MovieDetail)


    @Query("SELECT * FROM tbl_detail WHERE imdb_id=:imdbId")
    fun getDetail(imdbId:String): MovieDetail?
}