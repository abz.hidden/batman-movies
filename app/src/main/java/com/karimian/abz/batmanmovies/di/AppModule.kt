package com.karimian.abz.batmanmovies.di

import android.content.Context
import androidx.room.Room
import com.karimian.abz.batmanmovies.data.api.Api
import com.karimian.abz.batmanmovies.data.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    /**
     * build room database
     */
    @Provides
    @Singleton
    fun provideRoomDB(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(context, AppDatabase::class.java, "batman_movies_db")
        .fallbackToDestructiveMigration()
        .allowMainThreadQueries()
        .build()


    @Provides
    @Singleton
    fun provideMoviesDao(
        db: AppDatabase
    ) = db.moviesDao()


    @Provides
    @Singleton
    fun provideDetailDao(
        db: AppDatabase
    ) = db.DetailDao()


    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://www.omdbapi.com")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideApi(
        retrofit: Retrofit
    ): Api = retrofit.create(Api::class.java)

}