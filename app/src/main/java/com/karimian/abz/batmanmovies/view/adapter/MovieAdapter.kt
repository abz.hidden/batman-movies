package com.karimian.abz.batmanmovies.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.karimian.abz.batmanmovies.data.model.Movie
import com.karimian.abz.batmanmovies.databinding.MovieItemBinding


class MovieAdapter(
    private val listener : (movie : Movie) -> Unit
) : RecyclerView.Adapter<MovieAdapter.VH>() {

    private val list = arrayListOf<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(MovieItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    fun submitList(movies: List<Movie>) {
        this.list.addAll(movies)
        notifyDataSetChanged()
    }

    inner class VH(
        private val binding: MovieItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                listener(list[bindingAdapterPosition])
            }
        }

        fun bind(movie: Movie) {
            binding.apply {
                txtTitle.text = movie.title
                txtYear.text = movie.year
                Glide.with(imgPoster)
                    .load(movie.poster)
                    .into(imgPoster)
            }
        }

    }

}