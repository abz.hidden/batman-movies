package com.karimian.abz.batmanmovies.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.karimian.abz.batmanmovies.data.model.Movie
import com.karimian.abz.batmanmovies.data.model.MovieDetail

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movies: List<Movie>)

    @Query("SELECT * FROM tbl_movie")
    fun getList():  List<Movie>?

}