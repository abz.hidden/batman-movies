package com.karimian.abz.batmanmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tbl_detail")
data class MovieDetail(

    @ColumnInfo(name = "title")
    @SerializedName("Title") var title : String,

    @ColumnInfo(name = "year")
    @SerializedName("Year") var year : String,

    @ColumnInfo(name = "rated")
    @SerializedName("Rated") var rated : String,

    @ColumnInfo(name = "released")
    @SerializedName("Released") var released : String,

    @ColumnInfo(name = "runtime")
    @SerializedName("Runtime") var runtime : String,

    @ColumnInfo(name = "genre")
    @SerializedName("Genre") var genre : String,

    @ColumnInfo(name = "director")
    @SerializedName("Director") var director : String,

    @ColumnInfo(name = "writer")
    @SerializedName("Writer") var writer : String,

    @ColumnInfo(name = "actors")
    @SerializedName("Actors") var actors : String,

    @ColumnInfo(name = "plot")
    @SerializedName("Plot") var plot : String,

    @ColumnInfo(name = "language")
    @SerializedName("Language") var language : String,

    @ColumnInfo(name = "country")
    @SerializedName("Country") var country : String,

    @ColumnInfo(name = "awards")
    @SerializedName("Awards") var awards : String,

    @ColumnInfo(name = "poster")
    @SerializedName("Poster") var poster : String,

//    @ColumnInfo(name = "ratings")
//    @SerializedName("Ratings") var ratings : List<Ratings>,

    @ColumnInfo(name = "metascore")
    @SerializedName("Metascore") var metascore : String,

    @ColumnInfo(name = "imdb_rating")
    @SerializedName("imdbRating") var imdbRating : String,

    @ColumnInfo(name = "imdb_votes")
    @SerializedName("imdbVotes") var imdbVotes : String,

    @PrimaryKey
    @ColumnInfo(name = "imdb_id")
    @SerializedName("imdbID") var imdbID : String,

    @ColumnInfo(name = "type")
    @SerializedName("Type") var type : String,

    @ColumnInfo(name = "dvd")
    @SerializedName("DVD") var dvd : String,

    @ColumnInfo(name = "box_office")
    @SerializedName("BoxOffice") var boxOffice : String,

    @ColumnInfo(name = "production")
    @SerializedName("Production") var production : String,

    @ColumnInfo(name = "website")
    @SerializedName("Website") var website : String,

    @ColumnInfo(name = "response")
    @SerializedName("Response") var response : String

)
