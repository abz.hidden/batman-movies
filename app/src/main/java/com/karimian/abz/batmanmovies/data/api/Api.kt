package com.karimian.abz.batmanmovies.data.api

import com.karimian.abz.batmanmovies.data.model.MovieDetail
import com.karimian.abz.batmanmovies.data.model.SearchResponse
import com.karimian.abz.batmanmovies.helper.API_KEY
import retrofit2.http.GET
import retrofit2.http.Query


interface Api {

    @GET("/")
    suspend fun getMovieList(
        @Query("apikey") apiKey: String = API_KEY,
        @Query("s") id: String = "batman",
    ): SearchResponse

    @GET("/")
    suspend fun getMovieDetail(
        @Query("apikey") apiKey: String = API_KEY,
        @Query("i") imdbId: String,
    ): MovieDetail
}